﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp89
{
    class Program
    {
        static void Main(string[] args)
        {


            Random rand = new Random();
            int n = rand.Next(5, 10);
            Console.WriteLine($"JaggedLength={n}\n--------------");
            int[][] jagged = new int[n][];

            #region ArraysLength
            for (int i = 0; i < n; i++)
            {
                jagged[i] = new int[rand.Next(5, 15)];
                Console.Write($"{i}-st Array Lenght = {jagged[i].Length}\n ");

            }
            Console.WriteLine();

            #endregion


            #region fill massives

            Console.WriteLine("filled massive\n---------------");

            for (int i = 0; i < jagged.Length; i++)
            {
                for (int j = 0; j < jagged[i].Length; j++)
                {
                    jagged[i][j] = rand.Next(1, 100);
                    Console.Write($"{jagged[i][j]}  ");
                }
                Console.WriteLine();
            }
            Console.WriteLine("\nGet Sum Eache Massiv\n----------------");

            #endregion


            #region Get Sum Eache Massive and Gt Biggest Massive

            int max = jagged[0][0];
            int index = 0;
            for (int i = 0; i < jagged.Length; i++)
            {
                int sum = 0;
                for (int j = 1; j < jagged[i].Length; j++)
                {
                    sum += jagged[i][j];
                    if (max < sum)
                    {
                        max = sum;
                        index = i;
                    }
                }
                Console.WriteLine("Number {0} massiv's Sum = {1}", i, sum);
            }
                Console.WriteLine($"\n{index}-st massiv is biggest = {max}");

            #endregion
            Console.ReadLine();


        }
    }
}
